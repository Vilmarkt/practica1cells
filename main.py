
#Data treatment
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

#Models
from sklearn.svm import SVC
from sklearn.linear_model import *
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

#Metrics
from sklearn.metrics import r2_score
from sklearn.metrics import classification_report

import matplotlib.pyplot as plt

def realOutput(predictor, X):
    output = "idx,class"
    info = pd.DataFrame(pd.read_csv("test/info_test.csv")).to_numpy().flatten()
    prediction = predictor.predict(X)


    print(len(info))
    print(len(X))


    ccount = 0
    ecount = 0
    ocount = 0

    for i in range(0, len(info)):
        output += "\n" + str(info[i]) + ","
        if prediction[i] == 0:
            output += "c"
            ccount += 1
        if prediction[i] == 1:
            output += "e"
            ecount += 1
        if prediction[i] == 2:
            output += "o"
            ocount += 1

    print(" c: " + str(ccount) + " e: " + str(ecount) + " o: " + str(ocount))

    f = open("output.csv", "w")
    f.write(output)


color = pd.DataFrame(pd.read_csv("test/color_test.csv", sep=";", decimal=",")).iloc[:, :-1]
shape = pd.DataFrame(pd.read_csv("test/shape_test.csv", sep=";", decimal=","))
texture = pd.DataFrame(pd.read_csv("test/texture_test.csv", sep=";", decimal=",")).iloc[:, :-1]

data_pd = pd.concat([color, shape, texture], axis=1, ignore_index=True, sort=False)

true_test = data_pd.to_numpy()

color_train = pd.DataFrame(pd.read_csv("train/color_train.csv", decimal=","))
shape_train = pd.DataFrame(pd.read_csv("train/shape_train.csv", decimal=","))
texture_train = pd.DataFrame(pd.read_csv("train/texture_train.csv", decimal=","))

<<<<<<< HEAD
shape.describe().to_csv("test_shape_describe.csv")

shape_train.describe().to_csv("train_shape_describe.csv")

=======
>>>>>>> 6e33389d90c9249dc358d073a345e8559e187c8a
Ystr = (texture_train.iloc[: , -1:]).to_numpy().flatten()

color_train = color_train.iloc[: , :-1]
texture_train = texture_train.iloc[: , :-1]

train_data_pd = pd.concat([color_train, shape_train, texture_train], axis=1, ignore_index=True, sort=False)


X = train_data_pd.to_numpy()
Y = []


for i in range(0, len(Ystr)):
    if Ystr[i] == "c":
        Y.append(0)
    if Ystr[i] == "e":
        Y.append(1)
    if Ystr[i] == "o":
        Y.append(2)


<<<<<<< HEAD
"""
scaler = StandardScaler()
scaler.fit(np.append(X, true_test, axis= 0))
X = scaler.transform(X)
true_test = scaler.transform(true_test)
"""
=======



>>>>>>> 6e33389d90c9249dc358d073a345e8559e187c8a

#PCA
pca = PCA()
pca.fit(X)

points = np.arange(0, X.shape[1])

sum_arr = np.cumsum(pca.explained_variance_ratio_)
n_explain = 0

threshold = 0.99

for i in range(1, len(sum_arr)):
    if sum_arr[i-1] < threshold and sum_arr[i] >= threshold:
        n_explain = i

print("n_explain: " + str(n_explain))

pca = PCA(n_components=n_explain)
X = pca.fit_transform(X)
true_test = pca.fit_transform(true_test)

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.30, random_state=1)

scaler = StandardScaler()
scaler.fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)
true_test = scaler.transform(true_test)


scaler = StandardScaler()
scaler.fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.fit_transform(X_test)
true_test = scaler.transform(true_test)

ccount = 0
ecount = 0
ocount = 0

for i in range(0, len(y_train)):
    if y_train[i] == 0:
        ccount += 1
    if y_train[i] == 1:
        ecount += 1
    if y_train[i] == 2:
        ocount += 1

for i in range(0, len(y_test)):
    if y_train[i] == 0:
        ccount += 1
    if y_train[i] == 1:
        ecount += 1
    if y_train[i] == 2:
        ocount += 1

print(" c: " + str(ccount) + " e: " + str(ecount) + " o: " + str(ocount))


log = SGDClassifier(loss="log", eta0=0.001, max_iter=1000000, learning_rate="constant", random_state=5)
log.fit(X_train, y_train)
prediction = log.predict(X_test)

'''
#GRID SEARCH
<<<<<<< HEAD
param_dict = {"C" : np.arange(0.1, 5, 0.1), "kernel" : ["rbf"]}
grid_search = GridSearchCV(SVC(random_state=33), param_grid=param_dict, cv=None, verbose=1)
=======
param_dict = {"n_estimators" : range(50, 60, 10), "max_depth" : range(5, 15, 1)}
grid_search = GridSearchCV(RandomForestClassifier(random_state=33), param_grid=param_dict, cv=None, verbose=1)
>>>>>>> 6e33389d90c9249dc358d073a345e8559e187c8a
grid_search.fit(X_train, y_train)

print("Best params")
print(grid_search.best_params_)

svm = grid_search.best_estimator_
svm.fit(X_train, y_train)
prediction = svm.predict(X_test)
'''
print(classification_report(prediction, y_test))
print(r2_score(prediction, y_test))


realOutput(log, true_test)
